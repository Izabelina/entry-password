def let_inside(name):
    # wpuszcza tylko osoby zaproszone, które znają hasło.
    already_in = []
    while already_in != invited_people:
        given_name = input("Enter your name: ")
        if given_name in invited_people and given_name in already_in:
            print("That person is already inside.")
        elif given_name in invited_people and given_name not in already_in:
            given_password = input(f"{given_name}, enter password: ")
            if given_password == password:
                print(f"Wellcome {given_name}. Come inside!")
                already_in.append(given_name)
            else:
                print("Wrong password")
        else:
            print("You're not invited.")
    print("Everyone is already there.")

if __name__ == '__main__':
    invited_people = ["I", "H"]
    password = "C"
    invited_people = let_inside(invited_people)